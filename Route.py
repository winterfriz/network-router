import Network
import IPv4Address

class Route(object):
    def __init__(self, network, gateway, interfaceName, metric):
        self._network = network
        self._gateway = gateway
        self._interfaceName = interfaceName
        self._metric = metric

    def __eq__(self, other):
        return self.__dict__ == other.__dict__

    @property
    def gateway(self):
        return self._gateway

    @property
    def interfaceName(self):
        return self._interfaceName

    @property
    def metric(self):
        return self._metric

    @property
    def network(self):
        return self._network

    def __str__(self):
        if self._gateway == None:
            return "net: {}, interface: {}, metric: {}".format( \
                str(self._network), \
                self._interfaceName,
                self._metric
        )
        return "net: {} , gateway: {}, interface: {}, metric: {}".format( \
            str(self._network), \
            str(self._gateway), \
            self._interfaceName, \
            self._metric
        )
