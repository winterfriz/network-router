from IPv4Address import IPv4Address

class Network(object):
    __slots__ = ['_address', '_mask_length', '_mask', '_broadcast']

    def __init__(self, address, mask_length):
        self._address = IPv4Address(int(address) & ((2<<31) - (2<<(31-mask_length))))
        self._mask_length = mask_length
        self._mask = IPv4Address(((2<<31)-1) - ((2<<(31-self.mask_length))-1))
        self._broadcast = IPv4Address(((2<<(31-self.mask_length))-1) + int(self._address))

    def __eq__(self, other):
        return  self._address == other._address \
            and self._mask_length == other._mask_length \

    def __contains__(self, address):
        return int(self._broadcast) >= int(address) \
            and int(address) >= int(self._address)


    def __str__(self):
        return str(self._address) + "\\" + str(self._mask_length)

    @property
    def address(self):
        return self._address

    @property
    def broadcast_address(self):
        return self._broadcast

    @property
    def first_usable_address(self):
        if self._mask_length == 31:
            return self._address;
        if self._mask_length == 32:
            return None
        return IPv4Address(int(self._address)+1)

    @property
    def last_usable_address(self):
        if self._mask_length == 31:
            return self._broadcast;
        if self._mask_length == 32:
            return None;
        return IPv4Address(int(self._broadcast)-1)

    @property
    def mask_int(self):
        return int(self._mask)

    @property
    def mask_str(self):
        return str(self._mask)

    @property
    def mask_length(self):
        return self._mask_length

    @property
    def subnets(self):
        if self._mask_length == 32:
            return []
        new_mask = self.mask_length+1
        return [Network(self._address, new_mask) \
            , Network(IPv4Address(int(self._broadcast)+1), new_mask)]

    @property
    def total_hosts(self):
        if self._mask_length == 31:
            return 2;
        if self._mask_length == 32:
            return 0;
        return (2<<(31-self._mask_length))-2

    @property
    def is_public(self):
        for net in private_nets_ranges:
            if (int(net.address) <= int(self._address) \
                and int(net.broadcast_address) >= int(self._address)):
                
                return False
        return True

private_nets_ranges = [Network(IPv4Address('10.0.0.0'), 8),
    Network(IPv4Address('172.16.0.0'), 12),
    Network(IPv4Address('192.168.0.0'), 16)]

if __name__ == '__main__':
    address = IPv4Address("192.168.0.15")
    net = Network(address, 24)

    print(net)
    print(net.address)
    print(net.first_usable_address)
    print(net.last_usable_address)
    print(net.mask_int)
    print(net.mask_length)
    print(net.is_public)
    print(net.__contains__(IPv4Address("10.0.23.4")))
    print(net.__contains__(IPv4Address("192.168.0.25")))
    print(net.broadcast_address)
    print("total_hosts: %d" % net.total_hosts)

    subnets = net.subnets

    print(subnets[0])
    print(subnets[0].address)
    print(subnets[0].first_usable_address)
    print(subnets[0].last_usable_address)
    print(subnets[0].mask_length)
    print("total_hosts of subnets[0]: %d" % subnets[0].total_hosts)

    print()
    print(subnets[1])
    print(subnets[1].address)
    print(subnets[1].first_usable_address)
    print(subnets[1].last_usable_address)
    print(subnets[1].mask_length)

    # address = IPv4Address("192.168.0.0")
    # net = Network(address, 31)

    # print(net.address)
    # print("total_hosts: %d" % net.total_hosts)
    # print(net.first_usable_address)
    # print(net.last_usable_address)
    # print(net.broadcast_address)    # print(net.address)

    # subnets = net.subnets
    # print(subnets[1].address)
    # print("total_hosts: %d" % subnets[0].total_hosts)
    # print(subnets[1].first_usable_address)
    # print(subnets[1].last_usable_address)
    # print(subnets[1].broadcast_address)
