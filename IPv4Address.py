class IPv4Address(object):
    def __init__(self, address = []):
        if isinstance(address, str):
            self.address_int = self.str2int(address)
            self.address_str = address
        elif isinstance(address, int):
            self.address_int = address
            self.address_str = self.int2str(address)

    def int2str(self, address):
        if isinstance(address, int):
            if not (address >= 0 and address <= 4294967295):
                raise ValueError()

            parts = [str((address >> i) & 255) for i in (24, 16, 8, 0)]
            return ".".join(parts)
        else:
            raise ValueError()

    def str2int(self, address):
        if isinstance(address, str):
            parts = address.split(".")
            multiple = 256**3
            valid_address = 0

            for part in parts:
                if int(part) < 0 or int(part) > 255:
                    raise ValueError()
                valid_address += int(part) * multiple
                multiple //= 256
            return valid_address
        else:
            raise ValueError()

    def __lt__(self, address):
        return self.address_int < int(address)

    def __gt__(self, address):
        return self.address_int > int(address)

    def __eq__(self, address):
        return self.address_int == int(address)

    def __int__(self):
        return self.address_int

    def __str__(self):
        return self.address_str

if __name__ == '__main__':
    ip = IPv4Address("127.12.45.22")

    print(IPv4Address(2131504406))
    print(str(IPv4Address(2131504406)))
    print(int(IPv4Address("127.12.45.22")))

    print(ip == IPv4Address(2131504406))
    print(ip == IPv4Address(0xF834AD02))
    print(ip != IPv4Address(0xF834AD02))
    print(ip == IPv4Address("189.11.23.211"))
    print(ip > IPv4Address("131.16.34.66"))
    print(ip < IPv4Address("131.16.34.66"))

    print(ip.__dict__)
