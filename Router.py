from Network import Network
from IPv4Address import IPv4Address
from Route import Route

class Router(object):
    def __init__(self, routes):
        self._routes = self.__sort_routes(routes)

    def add_route(self, route):
        self._routes.append(route)
        self._routes = self.__sort_routes(self._routes)

    def remove_route(self, route):
        for item in self._routes:
            if item == route:
                self._routes.remove(item)

    def route_for_address(self, address):
        for route in self._routes:
            if address in route.network:
                return route
        return None

    def __sort_routes(self, routes):
        return sorted(routes, key = lambda route: (-route.network.mask_length, route.metric))

    @property
    def routes(self):
        return self._routes

if __name__ == '__main__':
    routes = [(Route(Network(IPv4Address("0.0.0.0"), 8), "192.168.0.1", "en0", 3)),
        (Route(Network(IPv4Address("192.168.0.0"), 19), None, "en0", 5)),
        (Route(Network(IPv4Address("10.0.0.0"), 8), "10.123.0.1", "en1", 4)),
        (Route(Network(IPv4Address("10.123.0.0"), 20), None, "en1", 3)),
        (Route(Network(IPv4Address("192.168.0.0"), 19), "192.168.0.1", "en1", 6))
    ]

    router = Router(routes)

    route = router.route_for_address(IPv4Address("192.168.0.176"))
    print(route.metric)
    print(route.interfaceName)
    net = route.network
    print(str(net))
    print(str(net.address))

    route = router.route_for_address(IPv4Address("192.168.0.0"))
    print(route.metric)
    print(route.interfaceName)
    net = route.network
    print(str(net))

    router.add_route(Route(Network(IPv4Address("1.1.0.0"), 17), "1.1.1.1", "en1", 10))
    router.remove_route(Route(Network(IPv4Address("10.0.0.0"), 8), "10.123.0.1", "en1", 10))

    print()
    for route in router.routes:
        print(route)
